using System;
using System.Reflection;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

using MyEntityFramework.Attributes;
using MyEntityFramework.Exceptions;

namespace MyEntityFramework.Core
{
    public class DbEntity<TEntity> where TEntity : class, new()
    {
        private readonly string _connectionString;
        private readonly ILogger<DbEntity<TEntity>> _logger;
        private readonly IDbClientProvider _dbClientProvider;
        private readonly string _loggingString;
        private readonly string _tTableName;
        private readonly PropertyInfo _tKeyPropertyInfo;
        private readonly string _tKeyColumnName; 
        private readonly List<PropertyInfo> _tPropertiesInfos;
        private readonly List<string?> _tColumnsNames;
        private readonly string?[] _columnsParams;
        private readonly string _whereParam;
        private readonly string _whereSql;
        private readonly string _insertSql;
        private readonly string _selectSql;
        private readonly string _selectWhereSql;
        private readonly string _selectLastKeySql;
        private readonly string _selectLastRowSql;
        private readonly string _updateSql;
        private readonly string _deleteSql;
        private readonly static object _semaphore = new object();

        internal DbEntity(
            string connectionString, 
            ILogger<DbEntity<TEntity>> logger, 
            IDbClientProvider dbClientProvider,
            Type contextType) 
        {
            _connectionString = connectionString;
            _logger = logger;
            _dbClientProvider = dbClientProvider;
            _loggingString = $"{contextType.Name}.DbEntity<{typeof(TEntity).Name}>";

            var entityType = typeof(TEntity);
            var tableAttr = entityType.GetCustomAttribute<TableAttribute>();

            if (tableAttr is null)
                if (string.IsNullOrEmpty(_tTableName))
                throw new AttributeUsageException(
                    entityType, 
                    tableAttr, 
                    $"Model that used as {nameof(TEntity)} must be marked with the {typeof(TableAttribute).Name}"
                );

            _tTableName = tableAttr.TableName;

            if (string.IsNullOrEmpty(_tTableName))
                throw new AttributeUsageException(
                    entityType, 
                    tableAttr, 
                    $"{nameof(TableAttribute.TableName)} Property of {typeof(TableAttribute).Name} must be not null and not empty"
                );

            _tPropertiesInfos = new List<PropertyInfo>();
            _tColumnsNames = new List<string?>();

            foreach (var property in entityType.GetProperties())
            {
                var attr = property.GetCustomAttribute<ColumnAttribute>();
                if (attr is not null)
                {
                    var cName = attr.ColumnName;

                    if (string.IsNullOrEmpty(cName))
                        throw new AttributeUsageException(
                            property, 
                            attr, 
                            $"{nameof(ColumnAttribute.ColumnName)} Property of {typeof(ColumnAttribute).Name} must be not null and not empty"
                        );

                    var pkAttr = property.GetCustomAttribute<PrimaryKeyAttribute>();
                    if (pkAttr is not null)
                    {
                        if (!string.IsNullOrEmpty(_tKeyColumnName))
                            throw new AttributeUsageException(
                                property,
                                pkAttr,
                                $"{typeof(PrimaryKeyAttribute).Name} can only be applied to one member"
                            );

                        _tKeyPropertyInfo = property;
                        _tKeyColumnName = cName;
                    }
                    _tPropertiesInfos.Add(property);
                    _tColumnsNames.Add(cName);
                }
            }

            _whereParam = "@__w_p_0";
            _whereSql = $"WHERE {_tKeyColumnName} = {_whereParam}";

            _columnsParams = new string[_tColumnsNames.Count];
            for (int i = 0; i < _columnsParams.Length; ++i)
            {
                _columnsParams[i] = $"@__p_{i}";
            }

            _insertSql = $"INSERT INTO {_tTableName} ({string.Join(", ", _tColumnsNames)}) VALUES ({string.Join(", ", _columnsParams)})";

            _selectSql = $"SELECT {string.Join(", ", _tColumnsNames)} FROM {_tTableName}";
            _selectWhereSql = $"{_selectSql} {_whereSql}";

            _selectLastKeySql = $"SELECT DISTINCT LAST_VALUE({_tKeyColumnName}) OVER () FROM {_tTableName}";
            _selectLastRowSql = $"{_selectSql} WHERE {_tKeyColumnName} = ({_selectLastKeySql})";

            var updates = new string[_tColumnsNames.Count];
            for (int j = 0; j < updates.Length; ++j)
            {
                updates[j] = $"{_tColumnsNames[j]} = {_columnsParams[j]}";
            }
            _updateSql =  $"UPDATE {_tTableName} SET {string.Join(", ", updates)} {_whereSql}";

            _deleteSql = $"DELETE FROM {_tTableName} {_whereSql}";

            _logger.LogDebug(
                $"{_loggingString} generated SQL with parameters:\n" + 
                $"      {_insertSql}\n" + 
                $"      {_selectSql}\n" +
                $"      {_selectWhereSql}\n" +
                $"      {_selectLastKeySql}\n" +
                $"      {_selectLastRowSql}\n" + 
                $"      {_updateSql}\n" +
                $"      {_deleteSql}"
            );
        }

        public bool Create(TEntity newValue)
        {
            int createdCount = 0;
            DbCommand cmd = _dbClientProvider.Command(_insertSql);

            for (int i = 0; i < _columnsParams.Length; ++i)
            {
                cmd.Parameters.Add(
                    _dbClientProvider.Parameter(
                        _columnsParams[i],
                        _dbClientProvider.Convert(_tPropertiesInfos[i].PropertyType),
                        _tPropertiesInfos[i].GetValue(newValue)
                    )
                );
            }

            lock (_semaphore)
            {
                using DbConnection conn = _dbClientProvider.Connection(_connectionString);
                cmd.Connection = conn;

                try { conn.Open(); }
                catch (DbException e) 
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to connect database for query:\n      " + cmd.CommandAsSql());
                    throw new DbConnectionFailedException(e);
                }

                try { createdCount = cmd.ExecuteNonQuery(); }
                catch (DbException e) 
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to execute query:\n      " + cmd.CommandAsSql());
                    throw new QueryExecutionException(e);
                }

            }
            
            _logger.LogInformation($"{_loggingString} successfully made query:\n      " + cmd.CommandAsSql());   

            return createdCount > 0;
        }

        public TEntity CreateGet(TEntity newValue)
        {
            TEntity? created = null;
            DbCommand cmd = _dbClientProvider.Command(_insertSql);

            for (int i = 0; i < _columnsParams.Length; ++i)
            {
                cmd.Parameters.Add(
                    _dbClientProvider.Parameter(
                        _columnsParams[i],
                        _dbClientProvider.Convert(_tPropertiesInfos[i].PropertyType),
                        _tPropertiesInfos[i].GetValue(newValue)
                    )
                );
            }

            lock (_semaphore)
            {
                using DbConnection conn = _dbClientProvider.Connection(_connectionString);
                cmd.Connection = conn;

                try { conn.Open(); }
                catch (DbException e) 
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to connect database for query:\n      " + cmd.CommandAsSql());
                    throw new DbConnectionFailedException(e);
                }

                DbDataReader? reader = null;

                try 
                { 
                    cmd.Transaction = conn.BeginTransaction();
                    _logger.LogInformation($"{_loggingString} begin transaction");

                    cmd.ExecuteNonQuery(); 
                    _logger.LogInformation($"{_loggingString} successfully made query:\n      " + cmd.CommandAsSql());

                    cmd.CommandText = _selectLastRowSql;
                    reader = cmd.ExecuteReader();
                    _logger.LogInformation($"{_loggingString} successfully made query:\n      " + cmd.CommandAsSql());

                    if (reader.Read())
                    {
                        created = ReadOne(reader);
                    }
                    
                    reader.Close();
                    cmd.Transaction.Commit();
                    _logger.LogInformation($"{_loggingString} commit transaction");
                }
                catch (DbException e) 
                {
                    reader?.Close();
                    cmd.Transaction?.Rollback();
                    _logger.LogError($"{_loggingString} rollback transaction, an error occurs while trying to execute query:\n      " + cmd.CommandAsSql());
                    throw new QueryExecutionException(e);
                }
                catch (Exception e)
                {
                    reader?.Close();
                    cmd.Transaction?.Rollback();
                    _logger.LogError($"{_loggingString} rollback transaction, unhandled exception");
                    throw new MyEntityFrameworkException(e);
                }
            }

            return created;
        }

        public IEnumerable<TEntity> Read(object? keyValue = null)
        {
            var resultList = new List<TEntity>();
            DbCommand cmd = _dbClientProvider.Command();

            if (keyValue is not null)
            {
                cmd.CommandText = _selectWhereSql;
                cmd.Parameters.Add(
                    _dbClientProvider.Parameter(
                        _whereParam, 
                        _dbClientProvider.Convert(_tKeyPropertyInfo.PropertyType), 
                        keyValue
                    )
                );
            }
            else
            {
                cmd.CommandText = _selectSql;
            }

            lock (_semaphore)
            {
                using DbConnection conn = _dbClientProvider.Connection(_connectionString);
                cmd.Connection = conn;

                try { conn.Open(); }
                catch (DbException e)
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to connect database for query:\n      " + cmd.CommandAsSql());
                    throw new DbConnectionFailedException(e);
                }

                DbDataReader? reader = null;

                try { reader = cmd.ExecuteReader(); }
                catch (DbException e) 
                {
                    reader?.Close();
                    _logger.LogError($"{_loggingString} an error occurs while trying to execute query:\n      " + cmd.CommandAsSql());
                    throw new QueryExecutionException(e);
                }

                try
                {
                    while (reader.Read())
                    {
                        resultList.Add(ReadOne(reader));
                    }
                }
                catch (Exception e) { throw new MyEntityFrameworkException(e); }
                finally { reader.Close(); }
            }

            return resultList;
        }

        public int Update(object key, TEntity newValue)
        {
            DbException? ex = null;
            int updated = 0;
            DbCommand cmd = _dbClientProvider.Command(_updateSql);

            for (int i = 0; i < _columnsParams.Length; ++i)
            {
                cmd.Parameters.Add(
                    _dbClientProvider.Parameter(
                        _columnsParams[i],
                        _dbClientProvider.Convert(_tPropertiesInfos[i].PropertyType),
                        _tPropertiesInfos[i].GetValue(newValue)
                    )
                );
            }
            cmd.Parameters.Add(
                _dbClientProvider.Parameter(
                    _whereParam,
                    _dbClientProvider.Convert(_tKeyPropertyInfo.PropertyType),
                    key
                )
            );

            lock (_semaphore)
            {
                using DbConnection conn = _dbClientProvider.Connection(_connectionString);
                cmd.Connection = conn;
                    
                try { conn.Open(); }
                catch (DbException e)
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to connect database for query:\n      " + cmd.CommandAsSql());
                    throw new DbConnectionFailedException(e);
                }
                
                try { updated = cmd.ExecuteNonQuery(); }
                catch (DbException e) 
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to execute query:\n      " + cmd.CommandAsSql());
                    throw new QueryExecutionException(e);
                } 
            }

            return updated;
        }

        public int Delete(object keyValue)
        {
            DbException? ex = null;
            int deleted = 0;
            DbCommand cmd = _dbClientProvider.Command(_deleteSql);

            cmd.Parameters.Add(
                _dbClientProvider.Parameter(
                    _whereParam,
                    _dbClientProvider.Convert(_tKeyPropertyInfo.PropertyType),
                    keyValue
                )
            );

            lock (_semaphore)
            {
                using DbConnection conn = _dbClientProvider.Connection(_connectionString);
                cmd.Connection = conn;
                    
                try { conn.Open(); }
                catch (DbException e)
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to connect database for query:\n      " + cmd.CommandAsSql());
                    throw new DbConnectionFailedException(e);
                }
                
                try { deleted = cmd.ExecuteNonQuery(); }
                catch (DbException e) 
                {
                    _logger.LogError($"{_loggingString} an error occurs while trying to execute query:\n      " + cmd.CommandAsSql());
                    throw new QueryExecutionException(e);
                }
            }

            return deleted;
        }

        private TEntity ReadOne(DbDataReader reader)
        {
            var readed = new TEntity();
            _tKeyPropertyInfo.SetValue(readed, reader[_tKeyColumnName]);
            for (int j = 0; j < _tColumnsNames.Count; ++j)
            {
                _tPropertiesInfos[j].SetValue(readed, reader[_tColumnsNames[j]]);
            }
            return readed;
        }
    }
}

internal static class CmdExtention
{
    internal static string CommandAsSql(this DbCommand cmd)
    {
        string query = cmd.CommandText;
        foreach (DbParameter p in cmd.Parameters)
        {
            query = query.Replace(p.ParameterName, p.Value?.ToString());
        }
        return query;   
    }
}