using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Data.SqlTypes;

namespace MyEntityFramework.Core
{
    public abstract class DbClientProviderBase<TCommand, TConnection, TParameter> 
        : IDbClientProvider
        where TCommand : DbCommand
        where TConnection : DbConnection
        where TParameter : DbParameter
    {
        protected readonly IDictionary<Type, DbType> TypesMap; 
        protected DbClientProviderBase()
        {
            TypesMap = new Dictionary<Type, DbType>()
            {
                { typeof(string), DbType.AnsiString },
                { typeof(SqlBinary), DbType.Binary },
                { typeof(byte), DbType.Byte }, { typeof(byte?), DbType.Byte },
                { typeof(bool), DbType.Boolean }, { typeof(bool?), DbType.Boolean },
                //DbType.Currency,
                { typeof(DateOnly), DbType.Date },
                { typeof(DateTime), DbType.DateTime },
                { typeof(decimal), DbType.Decimal }, { typeof(decimal?), DbType.Decimal },
                { typeof(double), DbType.Double }, { typeof(double?), DbType.Double },
                { typeof(Guid), DbType.Guid },
                { typeof(short), DbType.Int16 }, { typeof(short?), DbType.Int16 },
                { typeof(int), DbType.Int32 }, { typeof(int?), DbType.Int32 },
                { typeof(long), DbType.Int64 }, { typeof(long?), DbType.Int64 },
                { typeof(object), DbType.Object },
                { typeof(sbyte), DbType.SByte }, { typeof(sbyte?), DbType.SByte },
                { typeof(float), DbType.Single }, { typeof(float?), DbType.Single },
                //{ typeof(string), DbType.String },
                { typeof(TimeOnly), DbType.Time },
                { typeof(ushort), DbType.UInt16 }, { typeof(ushort?), DbType.UInt16 },
                { typeof(uint), DbType.UInt32 }, { typeof(uint?), DbType.UInt32 },
                { typeof(ulong), DbType.UInt64 }, { typeof(ulong?), DbType.UInt64 },
                //DbType.VarNumeric
                //Dbtype.AnsiStringFixedLength
                //DbType.StringFixedLength
                //DbType.Xml
                //DbType.DateTime2
                //Dbtype.DateTimeOffset
            };
        }

        public DbType Convert(Type typeToConvert) => TypesMap[typeToConvert];

#pragma warning disable
        public DbCommand Command() 
            => (DbCommand)Activator.CreateInstance(
                typeof(TCommand)
            );

        public DbCommand Command(string sql) 
        {
            var cmd = Command();
            cmd.CommandText = sql;
            return cmd;
        }

        public DbCommand Command(string sql, DbConnection conn)
        {
            var cmd = Command(sql);
            cmd.Connection = conn;
            return cmd;
        }

        public DbConnection Connection(string connectionString)
        {
            var conn = (DbConnection)Activator.CreateInstance(typeof(TConnection));
            conn.ConnectionString = connectionString;
            return conn;
        }

        public DbParameter Parameter(string name, DbType type, object? value = null)
        {
            var parameter = (DbParameter)Activator.CreateInstance<TParameter>();
            parameter.ParameterName = name;
            parameter.DbType = type;
            parameter.Value = value;
            return parameter;
        }      
#pragma warning restore

    }
}