using System;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace MyEntityFramework.Core
{
    public abstract class DbContextBase<TDbModel, TDbClientProvider> 
        : IDbContext<TDbModel>
        where TDbModel : DbModelBase, new()
        where TDbClientProvider : class, IDbClientProvider
    {
        public TDbModel DbModel { get; private set; }
        public DbContextBase(DbContextDependencies<TDbClientProvider> dependencies) 
        {  
            /* setting context database model and it's DAOs */
            DbModel = new TDbModel();

            /* all DbEntities of DbModel */
            var entityProps = DbModel.GetType().GetProperties(
                    BindingFlags.Instance | 
                    BindingFlags.Public | 
                    BindingFlags.NonPublic
                ).Where(p => 
                    p.PropertyType.GetGenericTypeDefinition() == typeof(DbEntity<>) && 
                    p.GetMethod is not null &&
                    p.GetMethod.IsPublic &&
                    p.SetMethod is not null &&
                    (p.SetMethod.IsPrivate || !p.SetMethod.IsPublic) // means private or protected
                );

            /* foreach DbEntity in DbModel */
            foreach(var prop in entityProps)
            {
                var pType = prop.PropertyType;

                /* get static extention method for generic logger creation */
                var entityCreateLoggerGenericMethod = 
                    typeof(LoggerFactoryExtensions).
                    GetMethods().Where(
                        m => m.IsGenericMethod && 
                        m.Name == nameof(LoggerFactoryExtensions.CreateLogger)
                    ).First().MakeGenericMethod(pType);

                /* creating generic logger for current DbEntity */
                var dbentityLogger = 
                    entityCreateLoggerGenericMethod.Invoke(
                        null, 
                        [dependencies.LoggerFactory]
                    );

                /* current DbEntity constructor */
                var ctor = pType.GetConstructors(
                    BindingFlags.Instance | 
                    BindingFlags.NonPublic
                ).First();

                /* invoking constructor to get current DbEntity */
                var dbEntity = ctor.Invoke(
                    [
                        dependencies
                            .Configuration
                            .GetConnectionString(ConnectionStringName),
                        dbentityLogger,
                        dependencies.DbClientProvider,
                        GetType()
                    ]
                );

                /* setting current DbEntity to DbModel */
                prop.SetValue(
                    DbModel, 
                    dbEntity
                );
                DbModel.DbEntities.Add(pType.GenericTypeArguments[0], prop);
            }
        }

        protected abstract string ConnectionStringName { get; }
    }
}