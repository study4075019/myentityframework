using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace MyEntityFramework.Core
{
    public class DbContextDependencies<TDbClientProvider>
        where TDbClientProvider : class, IDbClientProvider
    {
        public DbContextDependencies(
            IConfiguration cfg, 
            ILoggerFactory fac, 
            TDbClientProvider pvd)
        {
            Configuration = cfg;
            LoggerFactory = fac;
            DbClientProvider = pvd;
        }

        public IConfiguration Configuration { get; private set; }

        public ILoggerFactory LoggerFactory { get; private set; }

        public TDbClientProvider DbClientProvider { get; private set; }
    }
}