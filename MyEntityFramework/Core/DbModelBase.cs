using System;
using System.Reflection;
using System.Collections.Generic;

namespace MyEntityFramework.Core
{
    public abstract class DbModelBase
    {
        internal readonly IDictionary<Type, PropertyInfo> DbEntities;

        protected DbModelBase() => DbEntities = new Dictionary<Type, PropertyInfo>();

        public DbEntity<TEntity>? GetDbEntity<TEntity>() where TEntity : class, new()
        {
            if (DbEntities.TryGetValue(typeof(TEntity), out var dbEntityInfo))
            {
                return (DbEntity<TEntity>)dbEntityInfo.GetValue(this);
            }
            return null;
        }
    }
}