namespace MyEntityFramework.Core
{
    /// <summary>
    ///     <para>Factory that provides objects needed to access database and</para>
    ///     <para>work with it to <see cref="DbEntity{TDTO}"/>.</para>
    /// </summary>
    /// <remarks>
    ///     <para>Must be implemented for specific client such as <see cref="System.Data.SqlClient"/>.</para>
    /// </remarks>
    public interface IDbClientProvider
    {
        /// <summary>
        ///     <para>Used to map DTO's Property type to similar database type</para>
        ///     <para>for using it in <see cref="Parameter"/> method.</para>
        /// </summary>
        /// <param name="typeToConvert">
        ///     <see cref="System.Type"/> of User defined DTO's Property.
        /// </param>
        /// <returns>
        ///     <see cref="System.Data.DbType"/> used in <see cref="Parameter"/> method.
        /// </returns>
        System.Data.DbType Convert(System.Type typeToConvert);

        System.Data.Common.DbCommand Command();

        /// <summary>
        ///     Creates a command that must contains SQL query.
        /// </summary>
        /// <param name="sql">
        ///     SQL query that will be executed by <see cref="System.Data.Common.DbCommand"/>.
        /// </param>
        /// <returns>
        ///     <see cref="System.Data.Common.DbCommand"/> for executing <paramref name="sql"/>.
        /// </returns>
        System.Data.Common.DbCommand Command(string sql);

        /// <summary>
        ///     Creates a command that must contains SQL query and binded to specific connection.
        /// </summary>
        /// <param name="sql">
        ///     SQL query that will be executed by <see cref="System.Data.Common.DbCommand"/>.
        /// </param>
        /// <param name="conn">
        ///     Connection recieved from <see cref="Connection"/>.
        /// </param>
        /// <returns>
        ///     <see cref="System.Data.Common.DbCommand"/> for executing <paramref name="sql"/> connected to database with <paramref name="conn"/>.
        /// </returns>
        System.Data.Common.DbCommand Command(string sql, System.Data.Common.DbConnection conn);

        /// <summary>
        ///     Creates connection to database.
        /// </summary>
        /// <param name="connectionString">
        ///     Connection String specified for User implementation.
        /// </param>
        /// <returns>
        ///     <see cref="System.Data.Common.DbConnection"/> that used in <see cref="Command(string, System.Data.Common.DbConnection)"/>
        /// </returns>
        System.Data.Common.DbConnection Connection(string connectionString);

        /// <summary>
        ///     Creates Paramater that used in <see cref="System.Data.Common.DbCommand.Parameters"/>
        /// </summary>
        /// <param name="name">
        ///     ParameterName
        /// </param>
        /// <param name="type">
        ///     Parameter Type needed for used database management system
        /// </param>
        /// <param name="value">
        ///     ParameterValue
        /// </param>
        /// <returns></returns>
        System.Data.Common.DbParameter Parameter(string name, System.Data.DbType type, object? value = null);
    }
}