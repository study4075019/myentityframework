namespace MyEntityFramework.Core
{
    public interface IDbContext<TDbModel> // For Dependecy Injection
    {
        TDbModel DbModel { get; }
    }
}