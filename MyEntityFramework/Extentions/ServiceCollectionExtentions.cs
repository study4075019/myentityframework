using System;
using System.Linq;

using Microsoft.Extensions.DependencyInjection;

using MyEntityFramework.Core;

namespace MyEntityFramework.Extentions
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddMyEFContext<TContext>(this IServiceCollection services)
        {
            /* checking context type */
            var contextBase = typeof(TContext).BaseType;

            if (contextBase is null || 
                contextBase.GetGenericTypeDefinition() != typeof(DbContextBase<,>))
            {
                throw new InvalidOperationException();                
            }

            /* checking provider type */
            var providerType = contextBase.GenericTypeArguments.First(
                t => t.IsAssignableTo(typeof(IDbClientProvider))
            );
            var providerBase = providerType.BaseType;

            if (providerBase is null || 
                !providerBase.IsGenericType || 
                providerBase.GetGenericTypeDefinition() != typeof(DbClientProviderBase<,,>))
            {
                throw new InvalidOperationException();
            }

            /* adding provider */
            if (services.FirstOrDefault(serv => serv.ServiceType == providerType) is null)
            {
                services.AddSingleton(providerType);
            }

            /* adding context dependencies */
            var contextDepsImplType = typeof(DbContextDependencies<>).MakeGenericType(providerType);
            if (services.FirstOrDefault(serv => serv.ServiceType == contextDepsImplType) is null)
            {
                services.AddSingleton(contextDepsImplType);
            }

            /* adding database model */
            var dbModelType = contextBase.GenericTypeArguments.First(
                t => !t.IsAssignableTo(typeof(IDbClientProvider))
            );

            services.AddSingleton(
                typeof(IDbContext<>).MakeGenericType(dbModelType), 
                typeof(TContext)
            );

            return services;
        }
    }
}