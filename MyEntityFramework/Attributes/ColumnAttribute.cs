using System;

namespace MyEntityFramework.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ColumnAttribute : Attribute
    {
        public ColumnAttribute(string columnName)
        {
            if (columnName is null || columnName.Length == 0)
                throw new ArgumentException("", nameof(columnName));
            
            ColumnName = columnName;
        }
        public readonly string ColumnName;
    }
}