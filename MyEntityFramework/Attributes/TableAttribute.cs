using System;

namespace MyEntityFramework.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tableName) 
        {
            if (tableName is null || tableName.Length == 0)
                throw new ArgumentException("", nameof(tableName));

            TableName = tableName;
        }
        public readonly string TableName;
    }
}