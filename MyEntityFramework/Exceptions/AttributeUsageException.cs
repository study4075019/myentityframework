using System;
using System.Reflection;

namespace MyEntityFramework.Exceptions
{
    public class AttributeUsageException : MyEntityFrameworkException
    {
        public readonly MemberInfo MemberInfo;
        public readonly Attribute? Instance;
        public AttributeUsageException(
            MemberInfo memberInfo, 
            Attribute? instance, 
            string message) : base(message)
        {
            MemberInfo = memberInfo; 
            Instance = instance;
        }
    }
}