using System.Data.Common;

namespace MyEntityFramework.Exceptions
{
    public class QueryExecutionException : MyEntityFrameworkException
    {
        public QueryExecutionException(DbException dbEexception) : base(dbEexception) { }
    }
}