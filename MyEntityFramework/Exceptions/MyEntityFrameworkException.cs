using System;

namespace MyEntityFramework.Exceptions
{
    public class MyEntityFrameworkException : Exception 
    {
        public MyEntityFrameworkException(Exception exception)
            : base(exception.Message, exception) { }

        public MyEntityFrameworkException(string message) : base(message) { }

        public MyEntityFrameworkException() { }
    }
}