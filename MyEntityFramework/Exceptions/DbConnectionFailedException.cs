using System.Data.Common;

namespace MyEntityFramework.Exceptions
{
    public class DbConnectionFailedException : MyEntityFrameworkException
    {
        public DbConnectionFailedException(DbException dbException) : base(dbException) { }
    }
}